from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage, SignUp
from django.contrib.auth.models import User

# Create your tests here.
class NameloginUnitTest(TestCase):
    def test_welcome_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_is_exist(self):
         response = Client().get('/signup/')
         self.assertEqual(response.status_code, 200)

    def test_login_url_is_exist(self):
         response = Client().get('/accounts/login/')
         self.assertEqual(response.status_code, 200)
    
    def test_logout_url_is_exist(self):
         response = Client().get('/accounts/logout/')
         self.assertEqual(response.status_code, 302)

    def test_welcome_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'namelogin/homepage.html')

    def test_using_login_template(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_using_register_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'registration/signup.html')

    def test_post_forms(self):
        response = Client().get('/')
        self.assertIn('<a href="', response.content.decode())

    def test_url_ajax_get(self):
        response = self.client.get("/get_data/")
        self.assertEqual(response.status_code,200)

    def test_post_form(self):
        response = Client().get('/signup/')
        self.assertContains(response, '<form')
        
    def test_forms(self):
        data = {
            'username' : 'john',
            'password' : 'johndoeiscoming',
        }
        response = Client().post('/signup/', data)
        self.assertEqual(response.status_code, 200)
        content = {
            'username' : 'john',
            'password' : 'johndoeiscoming',
        }
        responseAfter = Client().post('/accounts/login', content)
        self.assertContains(response, 'john')

    def test_post(self):
         data = {
            'username' : 'test',
            'password' : 'dosenPPWpakadin'
         }
         response = Client().post('/accounts/login', data)
         self.assertEqual(response.status_code, 301)
