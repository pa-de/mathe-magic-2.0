from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views import generic
from django.http import JsonResponse
from django.contrib.auth.models import User
from mathgame.models import Point

# Create your views here.

def homepage(request):
    return render(request, 'namelogin/homepage.html')

def get_data(request):
    users = User.objects.all().count()
    if (Point.objects.filter(name=request.user.username).exists()):
        poin = Point.objects.get(name=request.user.username).points
    else:
        poin = 0
    return JsonResponse({'banyak_data': users, 'banyak_poin' : poin})

class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'    
