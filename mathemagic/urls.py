"""mathemagic URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
import mathgame.views
import discussion.views
import namelogin.views
import feedback.views

app_name = 'namelogin', 'mathgame', 'discussion', 'feedback'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('feedback/', include('feedback.urls')),
    path('mathgame/', mathgame.views.index, name='mathgame'),
    path('mathgame/postdata/', mathgame.views.postdata, name='postdata'),
    path('discussion/', include('discussion.urls')),
    path('', namelogin.views.homepage, name= 'landing-page'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('signup/', namelogin.views.SignUp.as_view(), name='signup'),
    path('get_data/', namelogin.views.get_data, name="get_data"),
]
