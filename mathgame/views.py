from django.shortcuts import render
from .models import Point, Result
from .forms import AnswerForm
import random
from django.http import JsonResponse

def index(request):
    angka_satu = str(random.randint(1,20))
    angka_dua = str(random.randint(1,20))
    operator = ['+', '-', '*']
    used_operator = operator[random.randint(0,2)]
    result = eval(angka_satu+used_operator+angka_dua)
    Result.objects.create(result=result)
    operasi = str(angka_satu+" "+used_operator+" "+angka_dua+" = ")
    form = AnswerForm()
   
    show = {
                    "angka_satu":angka_satu,
                    "operator":used_operator,
                    "angka_dua":angka_dua,
                    "answer_form": form,
                    "operasi":angka_satu+" "+used_operator+" "+angka_dua,
                    # 'points':str(orang.points),
                }
        
    return render(request,'mathgame/index.html', show)

def postdata(request):
    angka_satu = str(random.randint(1,20))
    angka_dua = str(random.randint(1,20))
    operator = ['+', '-', '*']
    used_operator = operator[random.randint(0,2)]
    result = eval(angka_satu+used_operator+angka_dua)
    Result.objects.create(result=result)
    operasi = str(angka_satu+" "+used_operator+" "+angka_dua+" = ")
    form = AnswerForm()
    answerNum = request.POST['answer']
    if (request.user.is_authenticated):
        nameName = request.user.username
    else:
        nameName = request.POST['name']

    
    
    
    if (int(answerNum)==int(Result.objects.order_by('-id')[1].result)):
        if (Point.objects.filter(name=nameName).exists()):
            
            orang = Point.objects.get(name=nameName)
            orang.points += 1                
            orang.save()
                
            
        else:
            
            orang = Point.objects.create(name=nameName,points=0)
            orang.points += 1
            orang.save()
            
    else:
        if (not Point.objects.filter(name=nameName).exists()):
            
            orang = Point.objects.create(name=nameName,points=0)
        else:
           
            orang = Point.objects.get(name=nameName)
    response_data = {}
    response_data['points']=str(orang.points)
    response_data['operasi']=str(angka_satu+" "+used_operator+" "+angka_dua+" = ")
    return JsonResponse(response_data)

# Create your views here.
