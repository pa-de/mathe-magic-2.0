from django.db import models

# Create your models here.
class Point(models.Model):
    name=models.CharField(max_length=50)
    points=models.IntegerField()

class Result(models.Model):
    result=models.IntegerField()