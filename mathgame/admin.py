from django.contrib import admin
from .models import Point, Result

admin.site.register(Point)
admin.site.register(Result)

# Register your models here.
