from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Point, Result

# Create your tests here.
class MathgameUnitTest(TestCase):
    def test_url_mathgame_exist(self):
        response = Client().get('/mathgame/')
        self.assertEqual(response.status_code, 200)
    
    def test_form(self):
        data = {
            'name':'praya',
            'answer':'1',
        }
        response = Client().post('/mathgame/postdata',data)
        self.assertEqual(response.status_code, 301)
    
    def test_mathgame_using_index_func(self):
        found = resolve('/mathgame/')
        self.assertEqual(found.func, index)
    
    def test_mathgame_using_mathgame_template(self):
        response = Client().get('/mathgame/')
        self.assertTemplateUsed(response, 'mathgame/index.html')
    
    def test_mathgame_Point_can_create_models(self):
        new_Point = Point.objects.create(name='praya', points=1)
        count = Point.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_mathgame_Result_can_create_models(self):
        new_Result = Result.objects.create(result=1)
        count = Result.objects.all().count()
        self.assertEqual(count, 1)
    



