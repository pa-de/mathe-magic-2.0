from django import forms

class AnswerForm(forms.Form):
    name = forms.CharField(label='Name : ', max_length=20, required=True, widget=forms.TextInput(attrs={
         'required':True, 'class':'form-control', 'id':'name'}))
    answer = forms.IntegerField(widget=forms.NumberInput(attrs={
         'required':True, 'id':'answer'
    }), label='')