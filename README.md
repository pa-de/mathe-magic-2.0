# mathe-magic

[![Pipeline](https://gitlab.com/pa-de/mathe-magic-2.0/badges/master/pipeline.svg)](https://gitlab.com/pa-de/mathe-magic-2.0/)
[![Coverage](https://gitlab.com/pa-de/mathe-magic-2.0/badges/master/coverage.svg)](https://gitlab.com/pa-de/mathe-magic-2.0/)


## Team Member
Abhipraya Tjondronegoro | 1806191742 | Information System <br>
Eugene Brigita Lauw | 1806141183 | Computer Science <br>
Khadijah Rizqy Mufida | 1806235712 | Computer Science <br>
Muhamad Adhytia Wana Putra Rahmadhan | 1806141321 | Computer Science

## Heroku
This is our heroku app link, check it out [here](https://mathe-magic.herokuapp.com//)!

## Idea of Our Project
Mathemagic is a website for kids to learn about math in an easy-going and encouraging environment. Through mathemagic, users such as students, parents, and teachers can track how well they are doing, check how others are doing, share their problems, and have a discussion with other users regarding math.

## Website Features
**Homepage and login (Khadijah)** Register your account to save the points <br>
**Mathgame (Praya)** Challenge yourself to solve math quiz <br>
**Discussion Room (Adhytia)** A place for users to share and give their opinions. Teachers can also interact with student through this feature <br>
**Feedback (Eugene)** Feature will be implemented with the aim of receiving complaints from users, regarding the material and existing features.
