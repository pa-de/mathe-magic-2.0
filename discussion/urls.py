from django.urls import path
from .views import discussion, discussionPost, discussionGet


app_name = 'discussion'

urlpatterns = [
    path('', discussion, name='discussion'),
    path('postdata/', discussionPost),
    path('getdata/', discussionGet),
]
