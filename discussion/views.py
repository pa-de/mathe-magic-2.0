from django.shortcuts import render
from .forms import discussionForms
from .models import discussionModels
from django.utils import timezone
from django.core import serializers
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

import json
import requests


def discussion(request):
    form = discussionForms()
    return render(request, 'discussion/discussion.html', {'form': form})

@csrf_exempt
def discussionPost(request):
    if request.method == "POST":
        form = discussionForms(request.POST)
        discussion = request.POST['discussion']
        name = request.POST['name']
        print("masuk if")
        model = discussionModels(discussion=discussion, name=name)
        model.save()

        discussion = list(discussionModels.objects.order_by('-time').values('discussion'))
        name = list(discussionModels.objects.order_by('-time').values('name'))
        time = list(discussionModels.objects.order_by('-time').values('time'))
        return JsonResponse({'discussionList': discussion, 'nameList': name, 'timeList': time})

def discussionGet(request):
    if request.method == "GET":
        discussion = list(discussionModels.objects.order_by('-time').values('discussion'))
        name = list(discussionModels.objects.order_by('-time').values('name'))
        time = list(discussionModels.objects.order_by('-time').values('time'))
        return JsonResponse({'discussionList': discussion, 'nameList': name, 'timeList': time})
