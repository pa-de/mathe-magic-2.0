from django.conf.urls import url
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'feedback'

urlpatterns = [
    path("", views.create, name="feedback"),
    path('masukan_data/',views.masukan_data),
    path('get_data/',views.get_data),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
