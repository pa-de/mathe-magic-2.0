from django.db import models

# Create your models here.
class PostModel(models.Model):
    name    = models.CharField(max_length=30)
    message = models.CharField(max_length=250)