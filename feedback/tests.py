from django.test import TestCase, Client
from django.urls import resolve
from .models import PostModel
from .forms import PostForm
from .views import masukan_data, get_data
from django.contrib.auth.models import User


# Create your tests here.
class Feedback(TestCase):
    def test_url_feedback_is_exist(self):
        response = Client().get("/feedback")
        self.assertEqual(response.status_code, 301)

    def test_feedback_html_is_used(self):
        response = Client().get("/feedback/")
        self.assertTemplateUsed(response, 'feedback/feedback.html')

    # def test_Post_Model_is_exist(self):
    #     new_feedback = PostModel.objects.create(message='test input status')
    #     self.assertEqual(PostModel.objects.all().count(), 1)
    #
    # def test_valid_Post_Form_and_saved(self):
    #     data = {
    #         'name': "eugenee",
    #         'message': "pls demi ppw!!",
    #     }
    #     form = PostForm(data=data)
    #     self.assertTrue(form.is_valid())
    #     form.save()
    #     self.assertEqual(PostModel.objects.all().count(), 1)
    #     self.assertEqual(PostModel.objects.get(id=1).message, 'pls demi ppw!!')

    # def test_dark_mode_toggle_is_exist(self):
    #     response = Client().get("/feedback")
    #     content = response.content.decode('utf8')
    #     self.assertIn("🌓", content)

    def test_view(self):
        data = {'name': "john",
                'message' : "ayo jadi"}
        form = PostForm(data=data)
        self.assertTrue(form.is_valid())

    def test_view_post(self):
        data = {'name': 'anon'}
        response = Client().post('/feedback', data)
        self.assertEqual(response.status_code, 301)

    def test_database(self):
        name = PostModel(name='bambang')
        name.save()
        count = PostModel.objects.all().count()
        self.assertEqual(count, 1)

    def test_url_ajax_post(self):
        data = {'name': 'eugene',
                'message': 'harus bisa'}
        response = self.client.post("/feedback/masukan_data/", data)
        self.assertEqual(response.status_code, 200)

    def test_url_ajax_get(self):
        response = self.client.get("/feedback/masukan_data/")
        self.assertEqual(response.status_code, 200)

    def test_ajax_get_views(self):
        response = resolve("/feedback/masukan_data/")
        self.assertEqual(response.func, masukan_data)

