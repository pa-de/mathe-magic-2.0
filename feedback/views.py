from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import HttpResponse,JsonResponse

# Create your views here.
from .forms import PostForm
from .models import PostModel

def show(request):
    posts = PostModel.objects.all()

    context = {
        'posts':posts,
    }

    return render(request, 'feedback/feedback-content.html', context)

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':  # POST request from browser
        if post_form.is_valid():
            post_form.save()
            return render(request, 'feedback/feedback.html')

    context = {
        'post_form': post_form,
    }

    return render(request, 'feedback/feedback.html', context)

def masukan_data(request):
    response_data = {}
    if request.method == "POST":
        if (request.user.is_authenticated):
            name = request.user.username
        else:
            name = request.POST['name']
        message = request.POST['message']
        model = PostModel(name= name,
                          message= message)
        model.save()
        response_data['name'] = name
        print(name)
    response_data['banyakData'] = PostModel.objects.all().count()
    return JsonResponse(response_data)

def get_data(request):
    name_list = list(PostModel.objects.values('name'))
    count = len(name_list)
    return JsonResponse({'banyakNama':count , 'name':name_list})
